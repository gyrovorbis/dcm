import bpy, array, os
from bpy_extras.io_utils import ExportHelper
from . import export_mesh_impl

class ExportSelectedMeshesOperator(bpy.types.Operator):
    
    bl_idname = 'dcmesh.export_selected_meshes'
    bl_label = 'Export Selected Meshes'
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        
        user_preferences = context.preferences
        addon_prefs = user_preferences.addons[__package__].preferences
        exported_meshes_path = bpy.path.abspath(addon_prefs.exported_meshes_path)

        if addon_prefs.exported_meshes_path == '':
            print('DCMesh Error: No export directory specified in addon preferences')
            return {"FINISHED"}

        if not os.path.exists(exported_meshes_path):
            os.makedirs(exported_meshes_path)

        to_export = []
    
        for obj in context.selected_objects:
            if obj.instance_type == 'COLLECTION':
                for child in obj.instance_collection.all_objects:
                    to_export.append(child)
            else:
                to_export.append(obj)

        for obj in to_export:
            name = obj.data.name if obj.data else obj.name
            export_mesh_impl.export_mesh_object(obj, os.path.join(exported_meshes_path, name + '.dcmesh'))

        return {"FINISHED"}